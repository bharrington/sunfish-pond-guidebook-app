//Tablet Mobile Img switch.
function adjustStyle(width) {
    width = parseInt(width);
    if (width < 767) {
        $('.mobile').removeClass("hidden");
        $('.tablet').addClass("hidden");
    } else if ((width >= 766)) {
        $('.mobile').addClass("hidden");
        $('.tablet').removeClass("hidden");
    }
}

$(document).ready( function() {
    adjustStyle($(this).width());
    $(window).resize(function() {
        adjustStyle($(this).width());
    });
});