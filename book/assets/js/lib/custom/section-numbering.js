// PREPEND SECTION NUMBER TO BOULDER LABEL
$(document).ready(function() {
  $(".long-valley .pg p").prepend(document.createTextNode("6"));
  $(".coal-run .pg p").prepend(document.createTextNode("7"));
  $(".falls-creek .pg p").prepend(document.createTextNode("8"));
  $(".triple-roofs .pg p").prepend(document.createTextNode("9"));
  $(".carbon-run .pg p").prepend(document.createTextNode("10"));
  $(".mccraney-run .pg p").prepend(document.createTextNode("11"));
  $(".sunfish-pond-main .pg p").prepend(document.createTextNode("12"));
  $(".sunfish-pond-annex .pg p").prepend(document.createTextNode("13"));
  $(".sunfish-pond-southside .pg p").prepend(document.createTextNode("14"));
  $(".sunfish-pond-tower .pg p").prepend(document.createTextNode("15"));
  $(".rock-run .pg p").prepend(document.createTextNode("16"));
  $(".off-the-path .pg p").prepend(document.createTextNode("17"));
});