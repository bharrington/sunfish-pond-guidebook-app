A comprehensive Northern Pennsylvania bouldering guide. Amazing sandstone boulders just over the NY PA border. Fully documented guide with over 200 problems and 17 boulderfields in this small region.

Features include:
- Detailed topo maps, google maps and grade metrics
- GPS coordinates
- Detailed boulder pages with all the beta
- Region overview and boulderfield overview
- Photo gallery, classic problem videos, developer stories & undocumented goodies